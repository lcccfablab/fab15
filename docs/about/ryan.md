# Ryan Corrigan

Ryan Corrigan is a laboratory instructional assistant at Lorain County Community
College’s Fab Lab. He began using the lab in 2014 as a student studying graphic
design, web design and photography. During his studies he decided he wanted to
learn about digital fabrication and utilized the Fab Lab software and equipment to teach
himself everything he could during his free time. Ryan loves to push the boundries
between art and technology by experimenting with new materials and techiques.
He is now one of two full time laboratory instructional assitants in the lab at LCCC. 
He excels in working with students as well as the public in teaching them the design 
process, laser functions, CNC routing, vinyl cutting, vacuum forming, & could be considered
an expert in CorelDraw and most Adobe software. Outside of work he enjoys volunteering, 
traveling, and freelancing as a photgrapher and designer.


